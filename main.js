var rsr = Raphael('mymap', '744.09448819', '1052.3622047');
var layer1 = rsr.set();
var rect2985 = rsr.rect(137.14285, 155.21933, 188.57143, 145.71428).attr({
    id: 'rect2985',
    x: '137.14285',
    y: '155.21933',
    parent: 'layer1',
    fill: '#dc0000',
    "fill-opacity": '1',
    stroke: 'none',
    'stroke-width': '1',
    'stroke-opacity': '1'
}).data('id', 'rect2985');
var rect29859 = rsr.rect(371.42856, 153.79076, 188.57143, 145.71428).attr({
    id: 'rect2985-9',
    x: '371.42856',
    y: '153.79076',
    parent: 'layer1',
    fill: '#009100',
    "fill-opacity": '1',
    stroke: 'none',
    'stroke-width': '1',
    'stroke-opacity': '1'
}).data('id', 'rect29859');
layer1.attr({
    'id': 'layer1',
    'name': 'layer1'
});
var rsrGroups = [layer1];
layer1.push(rect2985, rect29859);